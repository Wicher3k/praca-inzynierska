__author__ = 'Roland'
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
def wykresy(X1,Y1,X2,Y2,X3,Y3):

    plt.figure(1)
    plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
    plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
    x,=plt.plot(Y1,X1,'g^',label=u'Najwieksza wartosc funkcji celu')
    y,=plt.plot(Y2,X2,'b-',label=u'Srednia wartosc funkcji celu')
    z,=plt.plot(Y3,X3,'rv',label=u'Najmniejsza wartosc funkcji celu')
    plt.legend(handles=[x, y, z], loc=0, fontsize=10)

    plt.figure(2)
    plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
    plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
    x,= plt.plot(Y1,X1,'g--',label=u'Najwieksza wartosc funkcji celu')
    y,=plt.plot(Y2,X2,'b-',label=u'Srednia wartosc funkcji celu')
    z,=plt.plot(Y3,X3,'r-.',label=u'Najmniejsza wartosc funkcji celu')
    plt.legend(handles=[x, y, z], loc=0, fontsize=10)

    plt.figure(3)
    plt.xlabel('Iteracje',fontsize='large',fontweight='bold')
    plt.ylabel('Funkcja celu',fontsize='large',fontweight='bold')
    x,=plt.plot(Y1,X1,'g-',label=u'Najwieksza wartosc funkcji celu')
    y,=plt.plot(Y2,X2,'b-',label=u'Srednia wartosc funkcji celu')
    z,=plt.plot(Y3,X3,'r-',label=u'Najmniejsza wartosc funkcji celu')
    plt.legend(handles=[x, y, z], loc=0, fontsize=10)
    plt.show()


if  __name__ == '__main__':
    Y1=[1,2,3]
    X1=[3,4,5]
    Y2=[4.5,1,4]
    X2=[3,2,1]
    Y3=[1,6,7]
    X3=[2,4,6]
    wykresy(X1,Y1,X2,Y2,X3,Y3)