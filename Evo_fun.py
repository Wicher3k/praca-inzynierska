# -*- coding: utf-8 -*-
__author__ = 'Roland'
import random,time,copy,sys



def licz_os(L):
    liczba=0
    for i in range(len_os(L)):
        if L[i] is None:continue
        for j in range(len(L[i])):
            liczba+=1
    return liczba


def len_os(L):
    if 'przerwa' in L : return L.index("przerwa")
    else: return 0


def len_firm(L):
    return len(L)-len_os(L)-1


def sprawdzaniewarunkufirmy(M,L):
    if ((M[0])//14) != ((M[0]+M[1]-1)//14): return False
    dlugoscOs=len_os(L)
    for k in range(M[1]):
        suma=0
        for j in range(len_firm(L)):
            if (L[dlugoscOs+j+1][0]+L[dlugoscOs+j+1][1]-1< M[0]+k) or (M[0]+k< L[dlugoscOs+j+1][0]):continue
            suma=suma+L[dlugoscOs+1+j][2]
        if (suma+M[2])>6:return False
    return True


def inicjalizacja(nr_rodzica,dol_zakr,gor_zakr,liczbafirm,mplywacy,maksGodzinyFirma,maksToryFirma,dolnaCenaFirma,gornaCenaFirma,ILOSC_DNI):
    L=[]
    for i in range(ILOSC_DNI):
        if dol_zakr!=gor_zakr:
            liczbaos=random.randint(dol_zakr,gor_zakr)
        else:
            liczbaos=dol_zakr
        M=[]
        for j in range(liczbaos):
            M.append(random.choice(['Z','N','P']))
        L.append(M)
    L.append('przerwa')
    i=0
    while i <liczbafirm: #dodajemy  zlecenia firmy
        M=[random.randint(0,ILOSC_DNI-1),random.randint(1,maksGodzinyFirma),random.randint(1,maksToryFirma),random.randint(dolnaCenaFirma,gornaCenaFirma)] #![a,b,c,d] a-poczatek godziny doc:(0,ILOSC_DNI-1),b-ilosc godzin(1,6),c-ilosc torow(1,6),d-cena za h i za jeden tor

        czy=sprawdzaniewarunkufirmy(M,L)
        if czy is True:
            L.append(M)
            i+=1
    i=0
    while i<mplywacy:
        M=[random.randint(0,ILOSC_DNI-1),random.randint(1,2),1,0]
        czy=sprawdzaniewarunkufirmy(M,L)
        if czy is True:
            L.append(M)
            i+=1
    return L


def krzyzowanie(L1,L2,wersja,ILOSC_DNI):
    L3,L4=[],[]
    ilosc_os=len_os(L1)
    if wersja==1:
        gr=0
        while gr<1:

            skasujpetle=False
            L3=L1[:]
            L4=L2[:]
            LMAX=max(len(L3),len(L4))
            licz_osL4=len_os(L4)
            start_firm=licz_osL4+1
            X=random.randint(0,LMAX-1)
            if len(L3)>=len(L4):
                Y=random.randint(1,len(L3)-X)
                L3[X:X+Y],L4[X:X+Y]=L4[X:X+Y],L3[X:X+Y]
                if X==licz_osL4 and Y==1: continue
                if X+Y>=ilosc_os:
                    for i in range(len_firm(L3)):
                        M=L3[start_firm+i][:]
                        L3[start_firm+i]=[0,0,0,0]
                        if sprawdzaniewarunkufirmy(M,L3) is True: L3[start_firm+i]=M[:]
                        else:
                            skasujpetle=True
                            break
                    for i in range(len_firm(L4)):
                        if skasujpetle is True:break
                        M=L4[start_firm+i][:]
                        L4[start_firm+i]=[0,0,0,0]
                        if sprawdzaniewarunkufirmy(M,L4) is True:L4[start_firm+i]=M[:]
                        else:
                            skasujpetle=True
                            break
            else:
                Y=random.randint(1,len(L4)-X)
                L3[X:X+Y],L4[X:X+Y]=L4[X:X+Y],L3[X:X+Y]
                if X==len_os(L4) and Y==1: continue
                if (X+Y)>len_os(L4):
                    for i in range(len_firm(L4)):
                        M=L4[start_firm+i][:]
                        L4[start_firm+i]=[0,0,0,0]
                        if sprawdzaniewarunkufirmy(M,L4) is True: L4[start_firm+i]=M[:]
                        else:
                            skasujpetle=True;
                            break
                    for i in range(len_firm(L3)):
                        if skasujpetle is True:break
                        M=L3[start_firm+i][:]
                        L3[start_firm+i]=[0,0,0,0]
                        #print(M)
                        if sprawdzaniewarunkufirmy(M,L3) is True: L3[start_firm+i]=M[:]
                        else:
                            skasujpetle=True
                            break
            if skasujpetle is True:continue
            gr+=1
    elif wersja==2:
        L3=L1[:]
        L4=L2[:]

        pocz1=random.randint(0,ilosc_os-1)
        kon1=random.randint(0,ilosc_os-1)
        if pocz1>kon1:
            pocz1,kon1=kon1,pocz1
        L3[pocz1:kon1+1],L4[pocz1:kon1+1]=L4[pocz1:kon1+1],L3[pocz1:kon1+1]
        petla=0
        while petla<1:
            skasujpetle=False
            if len(L3)>=len(L4):
                pocz2=random.randint(ilosc_os+1,len(L3)-1)
                kon2=random.randint(ilosc_os+1,len(L3)-1)
                if pocz2 > kon2:
                      pocz2,kon2=kon2,pocz2
                L33=copy.copy(L3[pocz2:kon2+1])
                L44=copy.copy(L4[pocz2:kon2+1])
                L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L4[pocz2:kon2+1],L3[pocz2:kon2+1]
                for i in range(len(L3)):
                    if i<=ilosc_os: continue

                    M=L3[i][:]
                    L3[i]=[0,0,0,0]
                    #print(M)
                    czy=sprawdzaniewarunkufirmy(M,L3)
                    if czy is False:
                        L3[i]=M
                        L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L33,L44
                        skasujpetle=True
                        break
                    else: L3[i]=M[:]
                for i in range(len(L4)):
                    if skasujpetle is True:break
                    if i<=ilosc_os: continue
                    M=L4[i][:]
                    L4[i]=[0,0,0,0]
                    #print(M)
                    czy=sprawdzaniewarunkufirmy(M,L4)
                    if czy is False:
                        L4[i]=M
                        L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L33,L44
                        skasujpetle=True
                        break
                    else: L4[i]=M[:]

            else:
                pocz2=random.randint(ilosc_os+1,len(L4)-1)
                kon2=random.randint(ilosc_os+1,len(L4)-1)
                if pocz2 > kon2:
                      pocz2,kon2=kon2,pocz2
                L33=copy.copy(L3[pocz2:kon2+1])
                L44=copy.copy(L4[pocz2:kon2+1])
                L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L4[pocz2:kon2+1],L3[pocz2:kon2+1]
                for i in range(len(L4)):
                    if i<=ilosc_os: continue
                    M=L4[i][:]
                    L4[i]=[0,0,0,0]
                    czy=sprawdzaniewarunkufirmy(M,L4)
                    if czy is False:
                        L4[i]=M
                        L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L33,L44
                        skasujpetle=True
                        break
                    else: L4[i]=M[:]
                for i in range(len(L3)):
                    if skasujpetle is True:break
                    if i<=ilosc_os: continue
                    M=L3[i][:]
                    L3[i]=[0,0,0,0]
                    #print(M)
                    czy=sprawdzaniewarunkufirmy(M,L3)
                    if czy is False:
                        L3[i]=M
                        L3[pocz2:kon2+1],L4[pocz2:kon2+1]=L33,L44
                        skasujpetle=True
                        break
                    else: L3[i]=M[:]
            if skasujpetle is True:continue
            petla+=1
    elif wersja==3:
        ilosc_os=len_os(L1)
        for i in range(ilosc_os):
            if random.random()>0.5:
                L3.append(L1[i])
                L4.append(L2[i])
            else:
                L3.append(L2[i])
                L4.append(L1[i])
        L3.append("przerwa")
        L4.append("przerwa")
        for i in range(len_firm(L2)):
            x=random.random()
            czy11=sprawdzaniewarunkufirmy(L1[i+ilosc_os+1],L3)
            czy12=sprawdzaniewarunkufirmy(L2[i+ilosc_os+1],L4)
            czy21=sprawdzaniewarunkufirmy(L1[i+ilosc_os+1],L4)
            czy22=sprawdzaniewarunkufirmy(L2[i+ilosc_os+1],L3)
            if x>=0.5 and czy11 and czy12:
                #print(1)
                L3.append(L1[i+ilosc_os+1])
                L4.append(L2[i+ilosc_os+1])
            elif x<0.5 and czy21 and czy22:
                #print(2)
                L3.append(L2[i+ilosc_os+1])
                L4.append(L1[i+ilosc_os+1])
            elif czy11 and czy12:
                #print(3)
                L3.append(L1[i+ilosc_os+1])
                L4.append(L2[i+ilosc_os+1])
            elif czy21 and czy22:
                #print(4)
                L3.append(L2[i+ilosc_os+1])
                L4.append(L1[i+ilosc_os+1])
            else:
                if random.random()>=0.5:
                    kier=1
                else:
                    kier=-1
                M1=copy.copy(L1[i+ilosc_os+1])
                M2=copy.copy(L2[i+ilosc_os+1])
                for j in range(ILOSC_DNI+1):
                    M1[0]+=kier
                    M2[0]+=kier
                    if M1[0]==-1 and kier==-1:
                        M1[0]=ILOSC_DNI-1
                    if M2[0]==-1 and kier==-1:
                        M2[0]=ILOSC_DNI-1
                    if M1[0]==ILOSC_DNI and kier==1:
                        M1[0]=0
                    if M2[0]==ILOSC_DNI and kier==1:
                        M2[0]=0
                    czy11=sprawdzaniewarunkufirmy(M1,L3)
                    czy12=sprawdzaniewarunkufirmy(M2,L4)
                    czy21=sprawdzaniewarunkufirmy(M1,L4)
                    czy22=sprawdzaniewarunkufirmy(M2,L3)

                    if czy11 and czy12:
                        L3.append(M1)
                        L4.append(M2)

                        break
                    elif czy21 and czy22:
                        L3.append(M2)
                        L4.append(M1)

                        break
                    if j==ILOSC_DNI-1:
                        print "krzyzowanie v3 0000"
                        M1=[0,0,0,0]
                        M2=[0,0,0,0]

    else: print "nie wybrano wersji"
    return L3,L4

def mutacja(L,wersja,dol_zakr,gor_zakr,maksGodzinyFirma,maksToryFirma,dolnaCenaFirma,gornaCenaFirma,STALA_WARTOSC_CENY,ILOSC_DNI):
    ilosc_os=len_os(L)
    if wersja==1:
        s=0
        while s<1:
            X=random.randint(0,len(L)-1)

            if X<ilosc_os:
                ile_ludzi=random.randint(dol_zakr,gor_zakr)
                L[X]=[]
                for j in range(ile_ludzi):
                    L[X].append(random.choice(['Z','N','P']))
            elif X>ilosc_os:
                i=0
                while i<1:
                    if L[X][3]==0:
                        L[X]=[0,0,0,0]
                        M=[random.randint(0,ILOSC_DNI-1),random.randint(1,2),1,0]
                        czy=sprawdzaniewarunkufirmy(M,L)
                        if czy is False: continue
                        L[X]=M[:]
                        i+=1

                    else:
                        L[X]=[0,0,0,1]
                        M=[random.randint(0,ILOSC_DNI-1),random.randint(1,maksGodzinyFirma),random.randint(1,maksToryFirma),random.randint(dolnaCenaFirma,gornaCenaFirma)] #!![a,b,c,d] a-poczatek godziny doc:97-ILOSC_DNI,b-ilosc godzin,c-ilosc torow,d-cena za h i za jeden tor
                        czy=sprawdzaniewarunkufirmy(M,L)
                        if czy is False: continue
                        L[X]=M[:]
                        i+=1
            else:

                continue
            s+=1
    elif wersja==2:
        s=0
        while s<1:
            blad=0
            False_=False
            X=random.randint(0,len(L)-1)

            if X<ilosc_os:
                t=0
                while t<1:
                    Y=random.randint(0,ilosc_os-1)
                    if X==Y:
                        continue
                    L[X]=copy.copy(L[Y])
                    t+=1
            elif X>ilosc_os:
                t=0
                while t<1:
                    Y=random.randint(ilosc_os+1,len(L)-1)
                    if X==Y:continue
                    elif (L[Y][3]==0 and L[X][3]==0) or (L[Y][3]!=0 and L[X][3]!=0):
                        if STALA_WARTOSC_CENY:
                            los=random.randint(0,2)
                        else:
                            los=random.randint(0,3)
                            if L[Y][los]==0 and los==3: continue
                        zapamietaj=copy.copy(L[X])
                        M=copy.copy(L[X])
                        M[los]=L[Y][los]
                        L[X]=[0,0,0,0]
                        czy=sprawdzaniewarunkufirmy(M,L)
                        if not czy:
                            L[X]=zapamietaj
                            continue
                        L[X]=M
                    else:
                        blad+=1
                        if blad==5:
                            False_=True
                            break
                        continue
                    t+=1
            else:
                continue
            if False_:
                continue
            s+=1
    elif wersja==3:
        s=0
        while s<1:
            X=random.randint(0,len(L)-1)
            if X<ilosc_os:
                t=0
                while t<1:
                    Y=random.randint(0,ilosc_os-1)
                    if X==Y:
                        continue
                    temp=copy.copy(L[X])
                    L[X]=copy.copy(L[Y])
                    L[Y]=temp
                    t+=1
            elif X>ilosc_os:
                t=0
                while t<1:
                    Y=random.randint(ilosc_os+1,len(L)-1)
                    if X==Y:continue
                    elif L[Y][3]!=0 and L[X][3]!=0:
                        if STALA_WARTOSC_CENY:
                            los=random.randint(0,2)
                        else:
                            los=random.randint(0,3)
                        XX=copy.copy(L[X])
                        YY=copy.copy(L[Y])
                        M1,M2=copy.copy(L[X]),copy.copy(L[Y])
                        M1[los],M2[los]==copy.copy(L[Y][los]),copy.copy(L[X][los])
                        L[Y]=M2
                        L[X]=[0,0,0,0]

                        czy=sprawdzaniewarunkufirmy(M1,L)
                        if not czy:
                            L[X],L[Y]=XX,YY
                            continue

                        L[X]=M1
                        L[Y]=[0,0,0,0]
                        czy=sprawdzaniewarunkufirmy(M2,L)
                        if not czy:
                            L[X],L[Y]=XX,YY
                            continue
                        L[Y]=M2
                    elif L[X][3]==0 or L[Y][3]==0 and not(L[X][3]==0 and L[Y][3]==0) :
                        los=0
                        XX=copy.copy(L[X])
                        YY=copy.copy(L[Y])
                        M1,M2=copy.copy(L[X]),copy.copy(L[Y])
                        M1[los],M2[los]==copy.copy(L[Y][los]),copy.copy(L[X][los])
                        L[Y]=M2
                        L[X]=[0,0,0,0]
                        czy=sprawdzaniewarunkufirmy(M1,L)
                        if not czy:
                            L[X],L[Y]=XX,YY
                            continue
                        L[X]=M1
                        L[Y]=[0,0,0,0]
                        czy=sprawdzaniewarunkufirmy(M2,L)
                        if not czy:
                            L[X],L[Y]=XX,YY
                            continue
                        L[Y]=M2
                    else:
                        continue
                    t+=1
            else:
                continue
            s+=1
    elif wersja==4:
        s=0
        while s<1:
            X=random.randint(0,len(L)-1)

            if X<ilosc_os:
                Y=random.randint(0,1)

                if Y==1 and L[X+1]!="przerwa":
                    L[X],L[X+1]=copy.copy(L[X+1]),copy.copy(L[X])
                elif Y==0 and X-1>=0:
                    L[X],L[X-1]=copy.copy(L[X-1]),copy.copy(L[X])
                elif (Y==0 and X-1<0) or (Y==1 and L[X+1]=="przerwa"):
                    L[0],L[ilosc_os-1]=copy.copy(L[ilosc_os-1]),copy.copy(L[0])
                else:
                    print "error mutacja v4a"
            elif X>ilosc_os:
                t,dalej=0,0
                Y=random.randint(0,1)
                while t<1:
                    if STALA_WARTOSC_CENY:
                        los=random.randint(0,2)
                    else:
                        los=random.randint(0,3)

                    if Y==1:
                        if X+1+dalej<len(L):
                            if L[X+1+dalej][3]==0:los=0
                            XX,YY=copy.copy(L[X+dalej]),copy.copy(L[X+1+dalej])
                            M1,M2=copy.copy(L[X+dalej]),copy.copy(L[X+1+dalej])
                            M1[los],M2[los]=copy.copy(L[X+1+dalej][los]),copy.copy(L[X+dalej][los])
                            L[X+1+dalej]=M2
                            L[X+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M1,L)
                            if not czy:
                                L[X+dalej],L[X+1+dalej]=XX,YY
                                dalej+=1
                                continue
                            L[X+dalej]=M1
                            L[X+1+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M2,L)
                            if not czy:
                                L[X+dalej],L[X+1+dalej]=XX,YY
                                dalej+=1
                                continue
                            L[X+1+dalej]=M2

                        else:
                            los=0
                            XX,YY=copy.copy(L[X+dalej]),copy.copy(L[ilosc_os+1])
                            M1,M2=copy.copy(L[X+dalej]),copy.copy(L[ilosc_os+1])
                            M1[los],M2[los]=copy.copy(L[ilosc_os+1][los]),copy.copy(L[X+dalej][los])
                            L[ilosc_os+1]=M2
                            L[X+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M1,L)
                            if not czy:
                                L[X+dalej],L[ilosc_os+1]=XX,YY
                                X=ilosc_os+1
                                dalej=0
                                continue
                            L[X+dalej]=M1
                            L[ilosc_os+1]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M2,L)
                            if not czy:
                                L[X+dalej],L[ilosc_os+1]=XX,YY
                                X=ilosc_os+1
                                dalej=0
                                continue
                            L[ilosc_os+1]=M2

                    elif Y==0:
                        if L[X-1+dalej]!="przerwa":
                            if L[X+dalej][3]==0:los=0
                            XX,YY=copy.copy(L[X+dalej]),copy.copy(L[X-1+dalej])
                            M1,M2=copy.copy(L[X+dalej]),copy.copy(L[X-1+dalej])
                            M1[los],M2[los]=copy.copy(L[X-1+dalej][los]),copy.copy(L[X+dalej][los])
                            L[X-1+dalej]=M2
                            L[X+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M1,L)
                            if not czy:
                                L[X+dalej],L[X-1+dalej]=XX,YY
                                dalej-=1
                                continue
                            L[X+dalej]=M1
                            L[X-1+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M2,L)
                            if not czy:
                                L[X+dalej],L[X-1+dalej]=XX,YY
                                dalej-=1
                                continue
                            L[X-1+dalej]=M2
                        else:
                            if L[len(L)-1][3]==0: los=0
                            XX,YY=copy.copy(L[X+dalej]),copy.copy(L[len(L)-1])
                            M1,M2=copy.copy(L[X+dalej]),copy.copy(L[len(L)-1])
                            M1[los],M2[los]=copy.copy(L[len(L)-1][los]),copy.copy(L[X+dalej][los])
                            L[len(L)-1]=M2
                            L[X+dalej]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M1,L)
                            if not czy:
                                L[X+dalej],L[len(L)-1]=XX,YY
                                X=len(L)-1
                                dalej=0
                                continue
                            L[X+dalej]=M1
                            L[len(L)-1]=[0,0,0,0]
                            czy=sprawdzaniewarunkufirmy(M2,L)
                            if not czy:
                                L[X+dalej],L[len(L)-1]=XX,YY
                                X=len(L)-1
                                dalej=0
                                continue
                            L[len(L)-1]=M2
                    else:
                        print "error mutacjav4b"
                    t+=1
            else:
                continue
            s+=1
    else:
        print "nie wybrano wersji"


def migracja_ludzi(L):
    for i in range(len_os(L)):
        if L[i] is None:continue
        j=0
        dlugoscwektora=len(L[i])
        while j<dlugoscwektora:
            if L[i][j]=='A':
                L[i][j]='P'
                L[i].append('N')
            if L[i][j]=='dowyrzutu':
                L[i].remove('dowyrzutu')
                dlugoscwektora-=1
                j-=1
            j+=1


def wynik_funkcjicelu(L,cenaRano,cenaPopoludnie,cenaWieczor,zarobione_pieniadze,Ilosc_torow_dla_zwyklych_klientow,wspolczynnikKary):
    wynik,ile_zajetych_torow=0,[0,0,0,0,0,0,0]
    for i in range(len(L)):
        if L[i] is None: continue
        if L[i]=='przerwa': break
        ilosc_os_h=len(L[i])
        for k in range(7):
            if (5+k*14)>i>=(0+k*14):
                wynik+=cenaRano*ilosc_os_h
                break
            elif (10+k*14)>i>=(5+k*14):
                wynik+=cenaPopoludnie*ilosc_os_h
                break
            elif (14+k*14)>i>=(10+k*14):
                wynik+=cenaWieczor*ilosc_os_h
                break
            else: continue
    j=len_os(L)+1
    for i in range(len_firm(L)):

        if L[j][3]!= 0:
            wynik+=+L[j][3]*L[j][2]*L[j][1]
        for k in range(7):
            if (14+k*14)>L[j][0]>=(0+k*14):
                ile_zajetych_torow[k]+=L[j][2]*L[j][1]
                break
            else: continue
        j+=1
    czy_kara=False
    for i in range(len(ile_zajetych_torow)):
        if ile_zajetych_torow[i]>84-Ilosc_torow_dla_zwyklych_klientow:
            czy_kara=True
            break
    if czy_kara:
        wynik*=1-wspolczynnikKary

    wynik=round(wynik,2)
    zarobione_pieniadze+=wynik
    return zarobione_pieniadze,ile_zajetych_torow

def czas_trwania_pracy_basenu(Lpocz,ilosctygodni,cenaRano,cenaPopoludnie,cenaWieczor,dopuszczalna_ilosc,wspolczynnikKary,poprawaHumoru,pogorszenieHumoru):
    zarobione_pieniadze=0
    wektor_l_os=[]
    L=copy.deepcopy(Lpocz)
    dlugoscOs=len_os(L)
    dlugoscFirm=len_firm(L)
    zajete_tory=()
    for i in range(dlugoscOs):
        suma=0
        for j in range(dlugoscFirm):
            if i<L[dlugoscOs+j+1][0] or i>(L[dlugoscOs+j+1][0]+L[dlugoscOs+j+1][1]-1):continue
            suma+=L[dlugoscOs+j+1][2]
        zajete_tory+=(suma,)
    for tydzien in range(ilosctygodni):

        wektor_l_os.append(licz_os(L))
        if tydzien==(ilosctygodni-1):
            zli_ost,neu_ost,dob_ost=0,0,0
            for i in range(dlugoscOs):
                if L[i] is None:continue
                for j in range(len(L[i])):
                    if L[i][j]=="Z":
                        zli_ost+=1
                    elif L[i][j]=="N":
                        neu_ost+=1
                    elif L[i][j]=="P":
                        dob_ost+=1
            ostatni_ludzie=[dob_ost,neu_ost,zli_ost]
        for i in range(dlugoscOs):
            if zajete_tory[i]==6:
                L[i]=None
            else:
                for j in range(len(L[i])):
                    if (float(len(L[i]))/(6-zajete_tory[i]))>=poprawaHumoru:

                        if  L[i][j] is None:
                            continue
                        elif L[i][j] == 'N':
                            L[i][j] = 'Z'
                        elif L[i][j] == 'P':
                            L[i][j] = 'N'
                        elif L[i][j] == 'Z':
                            L[i][j] = 'dowyrzutu'
                        else:
                            print L[i][j]
                            print('BUG')
                    elif (float(len(L[i]))/(6-zajete_tory[i]))<=pogorszenieHumoru:
                        if  L[i][j] is None:
                            continue
                        elif L[i][j] == 'N':
                            L[i][j] = 'P'
                        elif L[i][j] == 'P':
                            L[i][j] = 'A'
                        elif L[i][j] == 'Z':
                            L[i][j] = 'N'
                        else:
                            print L[i][j]
                            print('BUG2')
        zarobione_pieniadze,ile_zajetych_torow=wynik_funkcjicelu(L,cenaRano,cenaPopoludnie,cenaWieczor,zarobione_pieniadze,dopuszczalna_ilosc,wspolczynnikKary)
        if tydzien!=(ilosctygodni-1):
            migracja_ludzi(L)

    return zarobione_pieniadze,ostatni_ludzie,wektor_l_os

def przechowuj_wszystkie_wyniki(L,cel,M,M_cele):
    for i in range(len(L)):
        M.append(L[i])
        M_cele.append(cel[i])
    return M,M_cele

def analiza_zysku2(cele):
    minn,maxx,srednia=100000000,0,0.0
    for cel in cele:
        if cel[0] > maxx: maxx=cel[0]
        elif cel[0] < minn: minn=cel[0]
        srednia+=cel[0]
    srednia/=len(cele)
    return minn, srednia, maxx

def wybierz_selekcje(L,wersja,cel):
    if wersja==1:
        mid=len(L)/2
        return L[mid:]
    elif wersja==2:
        for i in range(len(L)):
            for j in range(len(L)-1):
                if cel[j][0]<cel[j+1][0]:
                    L[j+1],L[j]=L[j],L[j+1]
                    cel[j+1],cel[j]=cel[j],cel[j+1]
        mid=len(L)/2
        return L[:mid]
    elif wersja==3:
        M=[]
        for i in range(len(L)):
            for j in range(len(L)-1):
                if cel[j][0]<cel[j+1][0]:
                    L[j+1],L[j]=L[j],L[j+1]
                    cel[j+1],cel[j]=cel[j],cel[j+1]
        G=list(range(len(L),0,-1))
        suma_przystosowan=0
        for i in G:
            suma_przystosowan +=i
        for i in range(len(G)):
            G[i] = float(G[i])/suma_przystosowan
        pelny=1
        for i in range(len(G)):
            G[i]=round(pelny-G[i],8)
            pelny=G[i]
        false_=True
        znajdzpolowe=1
        Wz=[]
        while znajdzpolowe<=len(L)//2:
            losuj=random.random()
            j=0
            for i in range(len(G)):
                if losuj>=G[j]:
                    if i in Wz:
                        false_=False
                        break
                    Wz.append(j)
                    M.append(L[j])
                    break
                j+=1
                if i==(len(G)-1):
                    "error"
            if not false_:
                false_=True
                continue
            znajdzpolowe+=1
        return M

    elif wersja==4:
        M,G=[],[]
        for i in range(len(L)):
            for j in range(len(L)-1):
                if cel[j][0]<cel[j+1][0]:
                    L[j+1],L[j]=L[j],L[j+1]
                    cel[j+1],cel[j]=cel[j],cel[j+1]
        suma_przystosowan=0
        najmnieszy_cel=cel[-1][0]*0.9
        przystosowanie=[]
        for x in range(len(cel)):
            ocena=cel[x][0]-najmnieszy_cel
            przystosowanie.append(ocena)
            suma_przystosowan+=ocena
        for i in range(len(przystosowanie)):
            G.append(float(przystosowanie[i])/suma_przystosowan)
        pelny=1
        for i in range(len(G)):
            G[i]=round(pelny-G[i],8)
            pelny=G[i]
        false_=True
        znajdzpolowe=1
        Wz=[]
        while znajdzpolowe<=len(L)//2:
            losuj=random.random()
            j=0
            for i in range(len(G)):
                if losuj>=G[j]:
                    if i in Wz:
                        false_=False
                        break
                    Wz.append(j)
                    M.append(L[j])
                    break
                j+=1
                if i==(len(G)-1):
                    "error"

            if false_==False:
                false_=True
                continue
            znajdzpolowe+=1
        return M
    elif wersja==5:
        M,seq=[],[]
        seq=list(range(0,len(L)))
        znajdzpolowe=1
        while znajdzpolowe<=len(L)//2:
            pierw=random.choice(seq)
            drugi=random.choice(seq)
            if pierw==drugi: continue
            jeden=seq.index(pierw)
            dwa=seq.index(drugi)
            if jeden>dwa:
                seq.pop(jeden)
                seq.pop(dwa)
            else:
                seq.pop(dwa)
                seq.pop(jeden)
            if cel[jeden][0]>cel[dwa][0]:
                seq.append(drugi)
                if jeden>dwa:
                    cel.pop(jeden)
                    x=cel.pop(dwa)
                else:
                    x=cel.pop(dwa)
                    cel.pop(jeden)
                cel.append(x)
                M.append(L[pierw])

            else:
                seq.append(pierw)
                if jeden>dwa:
                    x=cel.pop(jeden)
                    cel.pop(dwa)
                else:
                    cel.pop(dwa)
                    x=cel.pop(jeden)
                cel.append(x)
                M.append(L[drugi])
            znajdzpolowe+=1
        return M

def wyszukaj_maximum_celu(M,M_cele,ilosc_rodzicow):
    x=0
    for i in range(len(M_cele)):
        if M_cele[i] > x:
            x=M_cele[i]
            iteracja=i
            gl_iter=int(iteracja/(ilosc_rodzicow*2))+1

    print('najwieksza wartosc to ',x[0],'dla wektora: ',M[iteracja],'w glownej iteracji : ',gl_iter)
    return M[iteracja],x,gl_iter

def sortuj(L):
    poprzerwie=len_os(L)+1
    for x in range(len_firm(L)):
        for j in range(len_firm(L)-1):
            if L[j+poprzerwie][0]>L[j+poprzerwie+1][0]:
                L[j+poprzerwie+1],L[j+poprzerwie]=L[j+poprzerwie],L[j+poprzerwie+1]


if __name__ == '__main__':
    Lpocz=[["N","N","N","N","N","N","N","N","N","N","N","N","N","N","N","P"],["Z"],["Z"],["P"],["P","P"],'przerwa',[5,4,4,10],[1,2,2,10],[4,3,3,0]]
    L=[[['N'], ['N'], ['N'], ['N'], ['N'], 'przerwa', [3, 3, 4, 80], [0, 5, 1, 80], [3, 1, 1, 0]], [['Z'], ['Z'], ['Z'], ['Z'], ['Z'], 'przerwa', [2, 5, 6, 80], [0, 2, 1, 80], [0, 1, 1, 0]],[['P'], ['P'], ['P'], ['P'], ['P'], 'przerwa', [3, 3, 4, 80], [0, 5, 1, 80], [3, 1, 1, 0]], [['G'], ['G'], ['G'], ['G'], ['G'], 'przerwa', [2, 5, 6, 80], [0, 2, 1, 80], [0, 1, 1, 0]]]
    cel=[(2510,[1,1,1],["A"]), (2600,[2,2,2],["B"]), (2480,[3,3,3],["C"]),(2700,[2,2,2],['D'])]
    Lkrz=[[['Z', 'N'], ['P', 'P'], ['Z', 'P'], ['Z', 'N', 'N'], ['P', 'P'], 'przerwa', [6, 6, 2, 80], [0, 4, 1, 80], [1, 1, 1, 0], [3, 1, 1, 0]],[['P', 'Z'], ['Z', 'P'], ['N', 'N'], ['N', 'Z'], ['Z', 'P', 'P'], 'przerwa', [0, 3, 1, 80], [0, 5, 1, 80], [3, 2, 1, 0], [5, 1, 1, 0]]]
    G=[['N'], ['N'], ['N'], ['P'], ['Z'], ['Z'], ['Z'], ['Z'], ['P'], ['Z'], ['P'], ['Z'], ['P'], ['Z'], ['P'], ['Z'], ['P'], ['P'], ['N'], ['Z'], 'przerwa', [0, 5, 6, 50],[15,5,7,80]]
    czas_trwania_pracy_basenu(G,1,8,10,12,50)
    #L3,L4=krzyzowanie(Lkrz[0],Lkrz[1],3)
    #mutacja(Lpocz,4,2,3)
    #G=[["P","AAA","N","dowyrzutu"],["dowyrzutu"],['AAA'],["N"],'przerwa',[1,1,1,1]]
    #zarobione_pieniadze,ostatni_ludzie,wektor_l_os=czas_trwania_pracy_basenu(Lpocz,2)
    #print sprawdzaniewarunkufirmy([0,0,1,80],Lpocz)
    #LL=wybierz_selekcje(L,5,cel)
    #print inicjalizacja(1,2,3,5,5)
    #min,srednia,max=znajdz_do_wykresu(cel)
    #wyszukaj_maximum_celu(L,cel,4)
    #sortuj(Lpocz)
    global STALA_WARTOSC_CENY,ILOSC_DNI
    dolnaCenaFirma=1
    gornaCenaFirma=1
    if dolnaCenaFirma==gornaCenaFirma:
        STALA_WARTOSC_CENY = True
    else:
        STALA_WARTOSC_CENY = False
    ############# do poprawyyy ifa zrobic
    ILOSC_DNI = 70