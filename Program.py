# -*- coding: utf-8 -*-
from Tkinter import *
from AE_proces import proces
import tkFileDialog
import FileDialog

def przypiszRodzicow():
    mGui2.mainloop()
    ilosc_rodzicow=iloscRodzicowTK.get()
    if ilosc_rodzicow=="":
        ilosc_rodzicow=0
    return ilosc_rodzicow

def przypiszMutacje():
    mGui2.mainloop()
    prawdopodobienstwo_mutacji=prawdopMutacjaTK.get()
    if prawdopodobienstwo_mutacji=="":
        prawdopodobienstwo_mutacji=0
    return prawdopodobienstwo_mutacji

def przypiszIloscTyg():
    mGui2.mainloop()
    ilosc_tyg=iloscTygTK.get()
    if ilosc_tyg=="":
        ilosc_tyg=0
    return ilosc_tyg

def przypiszdolnyzakres():
    mGui2.mainloop()
    dol_zakr=dolnyzakresTK.get()
    if dol_zakr=="":
        dol_zakr=0
    return dol_zakr


def przypiszGornyZakres():
    mGui2.mainloop()
    gor_zakr=gornyzakresTK.get()
    if gor_zakr=="":
        gor_zakr=0
    return gor_zakr

def przypiszLiczbeFirm():
    mGui3.mainloop()
    liczbafirm=liczbafirmTK.get()
    if liczbafirm=="":
        liczbafirm=0
    return liczbafirm

def przypiszLiczbeSport():
    mGui3.mainloop()
    mplywacy=liczbasportTK.get()
    if mplywacy=="":
        mplywacy=0
    return mplywacy

def przypiszCeneRano():
    mGui2.mainloop()
    cenaRano=cenaRanoTK.get()
    if cenaRano=="":
        cenaRano=0
    return cenaRano

def przypiszCenePopo():
    mGui2.mainloop()
    cenaPopoludnie=cenaPopoludnieTK.get()
    if cenaPopoludnie=="":
        cenaPopoludnie=0
    return cenaPopoludnie

def przypiszCeneWiecz():
    mGui2.mainloop()
    cenaWieczor=cenaWieczorTK.get()
    if cenaWieczor=="":
        cenaWieczor=0
    return cenaWieczor

def przypiszpoprawaHumoru():
    mGui3.mainloop()
    poprawaHumoru=poprawaHumoruTK.get()
    if poprawaHumoru=="":
        poprawaHumoru=0
    return poprawaHumoru

def przypiszpogorszenieHumoru():
    mGui3.mainloop()
    pogorszenieHumoru=pogorszenieHumoruTK.get()
    if pogorszenieHumoru=="":
        pogorszenieHumoru=0
    return pogorszenieHumoru

def przypiszmaksGodzinyFirma():
    mGui3.mainloop()
    maksGodzinyFirma=maksGodzinyFirmaTK.get()
    if maksGodzinyFirma=="" or maksGodzinyFirma not in range(1,7):
        maksGodzinyFirma=1
    return maksGodzinyFirma

def przypiszmaksToryFirma():
    mGui3.mainloop()
    maksToryFirma=maksToryFirmaTK.get()
    if maksToryFirma=="" or maksToryFirma not in range(1,7):
        maksToryFirma=1
    return maksToryFirma

def przypiszdolnaCenaFirma():
    mGui3.mainloop()
    dolnaCenaFirma=dolnaCenaFirmaTK.get()
    if dolnaCenaFirma=="":
        dolnaCenaFirma=0
    return dolnaCenaFirma

def przypiszgornaCenaFirma():
    mGui3.mainloop()
    gornaCenaFirma=gornaCenaFirmaTK.get()
    if gornaCenaFirma=="":
        gornaCenaFirma=0
    return gornaCenaFirma

def przypiszIlosc_torow_dla_zwyklych_klientow():
    mGui3.mainloop()
    Ilosc_torow_dla_zwyklych_klientow=Ilosc_torow_dla_zwyklych_klientowTK.get()
    if Ilosc_torow_dla_zwyklych_klientow=="":
        Ilosc_torow_dla_zwyklych_klientow=0
    return Ilosc_torow_dla_zwyklych_klientow

def przypiszKare():
    mGui3.mainloop()
    WspKara=WspKaraTK.get()
    if WspKara=="":
        WspKara=0
    return WspKara




def selekcja():
    global wersja_selekcji
    wersja_selekcji =sel.get()

def krzyzowanie():
    global wersja_krzyz
    wersja_krzyz =krz.get()

def mutacja():
    global wersja_mutacja
    wersja_mutacja =mut.get()



def zakonczenie():
    def przypiszBlad():
        top.mainloop()
        blad_wzgledny=blad_wzglednyTK.get()
        if blad_wzgledny=="":
            blad_wzgledny=0
        return blad_wzgledny

    def przypiszIteracje():
        top.mainloop()
        iteracje=iteracjeTK.get()
        if iteracje=="":
            iteracje=0
        return iteracje

    def przypiszBrakZmiany():
        top.mainloop()
        ilosc_naj_do_zakonczenia=brakZmianyTK.get()
        if ilosc_naj_do_zakonczenia=="":
            ilosc_naj_do_zakonczenia=0
        return ilosc_naj_do_zakonczenia

    def kasujtop1():
        top.destroy()

    def kasujtop(event):
        top.destroy()

    global sposob_zakonczenia,blad_wzgledny,iteracje,ilosc_naj_do_zakonczenia,granicznaIteracja

    sposob_zakonczenia =zak.get()
    if sposob_zakonczenia==1:
        top = Toplevel()
        top.geometry('240x110+'+str(szerokosc+20)+'+'+str(wysokosc+200))
        top.title('Algorytm Ewolucyjny')
        mlabel7=Label(top,text='Podaj liczbę iteracji',fg='red',bg='gray88').pack()
        mEntry7=Entry(top,textvariable=iteracjeTK).pack()
        mlabel2=Label(top,text='').pack()
        top.bind("<Return>",kasujtop)
        mbutton = Button(top,text='OK',command = kasujtop1,fg='red',bg='white').pack()
        iteracje=przypiszIteracje()
    elif sposob_zakonczenia==2:
        top = Toplevel()
        top.geometry('240x180+'+str(szerokosc+20)+'+'+str(wysokosc+200))
        top.title('Algorytm Ewolucyjny')
        mlabel7=Label(top,text='Podaj błąd względny',fg='red',bg='gray88').pack()
        mEntry7=Entry(top,textvariable=blad_wzglednyTK).pack()
        mlabel2=Label(top,text='').pack()
        blad_wzglednyTK.set("0.0")
        mlabel7=Label(top,text='Podaj graniczną iterację',fg='red',bg='gray88').pack()
        mEntry7=Entry(top,textvariable=iteracjeTK).pack()
        mlabel2=Label(top,text='').pack()
        top.bind("<Return>",kasujtop)
        mbutton = Button(top,text='OK',command = kasujtop1,fg='red',bg='white').pack()
        blad_wzgledny=float(przypiszBlad())
        granicznaIteracja=przypiszIteracje()
        if blad_wzgledny>1:
            blad_wzgledny/=100
    elif sposob_zakonczenia==3:
        top = Toplevel()
        top.geometry('240x180+'+str(szerokosc+20)+'+'+str(wysokosc+200))
        top.title('Algorytm Ewolucyjny')
        mlabel7=Label(top,text='Podaj liczbę iteracji',fg='red',bg='gray88').pack()
        mEntry7=Entry(top,textvariable=brakZmianyTK).pack()
        mlabel2=Label(top,text='').pack()
        mlabel7=Label(top,text='Podaj graniczną iterację',fg='red',bg='gray88').pack()
        mEntry7=Entry(top,textvariable=iteracjeTK).pack()
        mlabel2=Label(top,text='').pack()
        top.bind("<Return>",kasujtop)
        mbutton = Button(top,text='OK',command = kasujtop1,fg='red',bg='white').pack()
        ilosc_naj_do_zakonczenia=przypiszBrakZmiany()
        granicznaIteracja=przypiszIteracje()
    else:
        print"error_zakonczenie"



def zaladujplik():
    global opcja,dane
    mGuifirst.fileName=tkFileDialog.askopenfilename(filetypes=(("Files",".txt"),("All Files","*.*")))
    print(mGuifirst.fileName)
    mGuifirst.destroy()
    opcja=1
    with open(mGuifirst.fileName,"r") as f:
        wiersze=f.readlines()
        dane=[]
        for wiersz in wiersze:
            dane.append(wiersz[:-1])

def konfiguracja_wlasna():
    global opcja
    opcja=2
    mGuifirst.destroy()

def kasuj():
    mGui.destroy()

def kasuj1(event):
    mGui.destroy()

def kasuj21():
    mGui2.destroy()

def kasuj22(event):
    mGui2.destroy()

def kasuj3(event):
    mGui3.destroy()

def kasuj31():
    mGui3.destroy()


mGuifirst=Tk()
screen_width = mGuifirst.winfo_screenwidth()
screen_height = mGuifirst.winfo_screenheight()
szerokosc=int(0.35*screen_width)
wysokosc=int(0.2*screen_height)
mGuifirst.geometry('370x50+'+str(szerokosc)+'+'+str(wysokosc))
mGuifirst.title('Algorytm Ewolucyjny')

mbutton = Button(text='Konfiguracja z pliku',command = zaladujplik,width=25,fg='red',bg='white').pack(side=LEFT)
mbutton = Button(text='Konfiguracja manualna',command = konfiguracja_wlasna,width=25,fg='red',bg='white').pack(side=LEFT)
mGuifirst.mainloop()

if opcja==2:

    wysokosc=int(0.1*screen_height)
    mGui=Tk()
    mGui.geometry('280x580+'+str(szerokosc)+'+'+str(wysokosc))
    mGui.title('Algorytm Ewolucyjny')

    blad_wzglednyTK = StringVar()
    brakZmianyTK=IntVar()
    iteracjeTK=IntVar()


    mlabel4=Label(text='Wybierz metodę selekcji',fg='red',bg='gray88').pack()
    sel = IntVar()
    Radio_1=Radiobutton(mGui,text='Wyłączona',value=1,variable=sel,command=selekcja).pack()
    Radio_2=Radiobutton(mGui,text='Elitarna',value=2,variable=sel,command=selekcja).pack()
    Radio_3=Radiobutton(mGui,text='Rankingowa',value=3,variable=sel,command=selekcja).pack()
    Radio_4=Radiobutton(mGui,text='Koła ruletki',value=4,variable=sel,command=selekcja).pack()
    Radio_5=Radiobutton(mGui,text='Turniejowa',value=5,variable=sel,command=selekcja).pack()
    mlabel2=Label(text='').pack()

    mlabell2=Label(text='Wybierz metodę krzyżowania',fg='red',bg='gray88').pack()
    krz = IntVar()
    Radio_6=Radiobutton(mGui,text='Liniowe',value=1,variable=krz,command=krzyzowanie).pack()
    Radio_7=Radiobutton(mGui,text='Liniowe podwójne',value=2,variable=krz,command=krzyzowanie).pack()
    Radio_8=Radiobutton(mGui,text='Jednorodne',value=3,variable=krz,command=krzyzowanie).pack()
    mlabel2=Label(text='').pack()

    mlabell3=Label(text='Wybierz metodę mutacji',fg='red',bg='gray88').pack()
    mut = IntVar()
    Radio_9=Radiobutton(mGui,text='Równomierna',value=1,variable=mut,command=mutacja).pack()
    Radio_10=Radiobutton(mGui,text='W oparciu o pozycje',value=2,variable=mut,command=mutacja).pack()
    Radio_11=Radiobutton(mGui,text='Pojedyncza zamiana',value=3,variable=mut,command=mutacja).pack()
    Radio_12=Radiobutton(mGui,text='Przyległa wymiana',value=4,variable=mut,command=mutacja).pack()
    mlabel2=Label(text='').pack()

    mlabell3=Label(text='Wybierz metodę zakończenia',fg='red',bg='gray88').pack()
    zak = IntVar()
    Radio_13=Radiobutton(mGui,text='Określona liczba iteracji',value=1,variable=zak,command=zakonczenie).pack()
    Radio_14=Radiobutton(mGui,text='Błąd względny',value=2,variable=zak,command=zakonczenie).pack()
    Radio_15=Radiobutton(mGui,text='Stały wynik przez daną ilość iteracji',value=3,variable=zak,command=zakonczenie).pack()
    mlabel2=Label(text='').pack()

    mGui.bind("<Return>",kasuj1)
    mbutton = Button(text='OK',command = kasuj,fg='red',bg='white').pack()

    mGui.mainloop()
    try:
        print wersja_selekcji,"wersja_selekcji"
    except:
        print "nie przypisano wersji selekcji"
    try:
        print wersja_krzyz,"wersja_krzyz"
    except:
        print "nie przypisano wersji krzyżowania"
    try:
        print wersja_mutacja,"wersja_mutacja"
    except:
        print "nie przypisano wersji mutacji"
    try:
        print sposob_zakonczenia,"sposob_zakonczenia"
    except:
        print "nie przypisano wersji zakonczenia"
    try:
        print blad_wzgledny,'blad_wzgledny'
    except:
        blad_wzgledny=0
    try:
        print ilosc_naj_do_zakonczenia,'ilosc_naj_do_zakonczenia'
    except:
        ilosc_naj_do_zakonczenia=0

    try:
        print iteracje,'iteracje'
    except:
        iteracje=0
    try:
        print granicznaIteracja,'grancizne iteracje'
    except:
        granicznaIteracja=0




    mGui2=Tk()
    mGui2.geometry('280x530+'+str(szerokosc)+'+'+str(wysokosc))
    mGui2.title('Algorytm Ewolucyjny')

    dolnyzakresTK,gornyzakresTK=IntVar(),IntVar()
    iloscRodzicowTK,prawdopMutacjaTK,iloscTygTK = IntVar(),StringVar(),IntVar()
    cenaRanoTK,cenaPopoludnieTK,cenaWieczorTK=StringVar(),StringVar(),StringVar()


    mlabel1=Label(text='Podaj parzystą liczbę rodziców',fg='red',bg='gray88').pack()
    mEntry1=Entry(textvariable=iloscRodzicowTK).pack()
    mlabel2=Label(text='').pack()

    mlabel7=Label(text='Podaj prawdopodobieństwo mutacji',fg='red',bg='gray88').pack()
    mEntry7=Entry(textvariable=prawdopMutacjaTK).pack()
    mlabel2=Label(text='').pack()
    prawdopMutacjaTK.set("0.0")

    mlabel3=Label(text='Podaj czas w tygodniach działności basenu',fg='red',bg='gray88').pack()
    mEntry2=Entry(textvariable=iloscTygTK).pack()
    mlabel2=Label(text='').pack()

    Label(text='Podaj cenę wstępu w godzinach porannych',fg='red',bg='gray88').pack()
    Entry(textvariable=cenaRanoTK).pack()
    Label(text='').pack()
    cenaRanoTK.set("0")

    Label(text='Podaj cenę wstępu w godzinach popoludniowych',fg='red',bg='gray88').pack()
    Entry(textvariable=cenaPopoludnieTK).pack()
    Label(text='').pack()
    cenaPopoludnieTK.set("0")

    Label(text='Podaj cenę wstępu w godzinach wieczornych',fg='red',bg='gray88').pack()
    Entry(textvariable=cenaWieczorTK).pack()
    Label(text='').pack()
    cenaWieczorTK.set("0")

    mlabel1=Label(text='Podaj dolny zakres ilości osob w godzinie',fg='red',bg='gray88').pack()
    mEntry1=Entry(textvariable=dolnyzakresTK).pack()
    mlabel2=Label(text='').pack()

    mlabel7=Label(text='Podaj górny zakres ilosci osob w godzinie',fg='red',bg='gray88').pack()
    mEntry7=Entry(textvariable=gornyzakresTK).pack()
    mlabel2=Label(text='').pack()


    mGui2.bind("<Return>",kasuj22)
    mbutton = Button(text='OK',command = kasuj21,fg='red',bg='white').pack()


    ilosc_rodzicow=przypiszRodzicow()
    prawdopodobienstwo_mutacji=float(przypiszMutacje())
    ilosc_tyg=przypiszIloscTyg()
    cenaRano=float(przypiszCeneRano())
    cenaPopoludnie=float(przypiszCenePopo())
    cenaWieczor=float(przypiszCeneWiecz())
    dol_zakr=przypiszdolnyzakres()
    gor_zakr=przypiszGornyZakres()




    mGui2.mainloop()


    mGui3=Tk()
    maksGodzinyFirmaTK,maksToryFirmaTK,dolnaCenaFirmaTK,gornaCenaFirmaTK,poprawaHumoruTK,pogorszenieHumoruTK=IntVar(),IntVar(),IntVar(),IntVar(),StringVar(),StringVar()
    liczbafirmTK,liczbasportTK,Ilosc_torow_dla_zwyklych_klientowTK,WspKaraTK = IntVar(),IntVar(),StringVar(),StringVar()
    mGui3.geometry('352x660+'+str(szerokosc)+'+'+str(wysokosc))
    mGui3.title('Algorytm Ewolucyjny')

    Label(text='Podaj liczbę osób/tor, gdy nastąpi poprawa nastawienia klienta:',fg='red',bg='gray88').pack()
    Entry(textvariable=poprawaHumoruTK).pack()
    Label(text='').pack()
    poprawaHumoruTK.set("0.0")

    Label(text='Podaj liczbę osób/tor, gdy nastąpi pogorszenie nastawienia klienta:',fg='red',bg='gray88').pack()
    Entry(textvariable=pogorszenieHumoruTK).pack()
    Label(text='').pack()
    pogorszenieHumoruTK.set("0.0")

    mlabel3=Label(text='Podaj ilość firm zgłaszających swoje propozycje',fg='red',bg='gray88').pack()
    mEntry2=Entry(textvariable=liczbafirmTK).pack()
    mlabel2=Label(text='').pack()

    Label(text='Podaj maksymalną ilość godzin jednej rezerwcji firm (1-6):',fg='red',bg='gray88').pack()
    Entry(textvariable=maksGodzinyFirmaTK).pack()
    Label(text='').pack()
    cenaWieczorTK.set("0")

    Label(text='Podaj maksymalną ilość torów jednej rezerwcji firm (1-6)',fg='red',bg='gray88').pack()
    Entry(textvariable=maksToryFirmaTK).pack()
    Label(text='').pack()
    cenaWieczorTK.set("0")

    Label(text='Podaj minimalną cenę za 1tor/1h rezerwacji firm',fg='red',bg='gray88').pack()
    Entry(textvariable=dolnaCenaFirmaTK).pack()
    Label(text='').pack()
    cenaWieczorTK.set("0")

    Label(text='Podaj maksymalną cenę za 1tor/1h rezerwacji firm',fg='red',bg='gray88').pack()
    Entry(textvariable=gornaCenaFirmaTK).pack()
    Label(text='').pack()
    cenaWieczorTK.set("0")

    mlabel3=Label(text='Podaj ilość sportowców',fg='red',bg='gray88').pack()
    mEntry2=Entry(textvariable=liczbasportTK).pack()
    mlabel2=Label(text='').pack()

    Label(text='Podaj procent najmniejszego wykorzystania torów na dzień',fg='red',bg='gray88').pack()
    Label(text='wyłącznie przez klientów *[%] (kara pieniężna)',fg='red',bg='gray88').pack()
    mEntry2=Entry(textvariable=Ilosc_torow_dla_zwyklych_klientowTK).pack()
    mlabel2=Label(text='').pack()
    Ilosc_torow_dla_zwyklych_klientowTK.set("0")

    Label(text='Współczynnik kary',fg='red',bg='gray88').pack()
    mEntry2=Entry(textvariable=WspKaraTK).pack()
    mlabel2=Label(text='').pack()
    WspKaraTK.set("0.0")

    mGui3.bind("<Return>",kasuj3)
    mbutton = Button(text='OK',command = kasuj31,fg='red',bg='white').pack()
    Label(text='').pack()
    Label(text='* Ilość torów/h w jednym dniu 6torów*14h=84[tor/h]',fg='black',bg='gray88',font=('Helvetica',8)).pack()

    poprawaHumoru=float(przypiszpoprawaHumoru())
    pogorszenieHumoru=float(przypiszpogorszenieHumoru())
    liczbafirm=przypiszLiczbeFirm()
    maksGodzinyFirma=przypiszmaksGodzinyFirma()
    maksToryFirma=przypiszmaksToryFirma()
    dolnaCenaFirma=przypiszdolnaCenaFirma()
    gornaCenaFirma=przypiszgornaCenaFirma()
    mplywacy=przypiszLiczbeSport()
    Ilosc_torow_dla_zwyklych_klientow=int(round(float(przypiszIlosc_torow_dla_zwyklych_klientow())/100*84))
    wspolczynnikKary=float(przypiszKare())
    mGui3.mainloop()

    print ilosc_rodzicow,"ilosc_rodzicow"
    print prawdopodobienstwo_mutacji,"prawdopodobienstwo_mutacji"
    print ilosc_tyg,'ilosc_tyg'
    print cenaRano,'cena ranna'
    print cenaPopoludnie,'cena popołudniowa'
    print cenaWieczor,'cena wieczorna'
    print dol_zakr,'dolny zakres ludzi'
    print gor_zakr,'gorny zakres ludzi'
    print poprawaHumoru,"os/tory poprawa"
    print pogorszenieHumoru, "os/tory pogorszenie"
    print liczbafirm,'liczba firm'
    print maksGodzinyFirma,'maksGodzinyFirma'
    print maksToryFirma,'maksToryFirma'
    print dolnaCenaFirma,'dolnaCenaFirma'
    print gornaCenaFirma,'gornaCenaFirma'
    print mplywacy,'liczba plywakow'
    print Ilosc_torow_dla_zwyklych_klientow,'Ilosc_torow_dla_zwyklych_klientow'
    print wspolczynnikKary, "wspolczynnik kary"




if opcja==1:
    ilosc_rodzicow=int(dane[0])
    ilosc_tyg=int(dane[1])
    if ilosc_tyg==1:
        tyg=" tydzień"
    elif ilosc_tyg in (2,3,4):
        tyg=" tygodnie"
    else:
        tyg=" tygodni"

    prawdopodobienstwo_mutacji=float(dane[2])
    iteracje=int(dane[3])

    wersja_selekcji=int(dane[4])
    i = range(1,6)
    if i[0]==wersja_selekcji:
        metoda_selekcji="Wyłączona"
    elif i[1]==wersja_selekcji:
        metoda_selekcji="Elitarna"
    elif i[2]==wersja_selekcji:
        metoda_selekcji="Rankingowa"
    elif i[3]==wersja_selekcji:
        metoda_selekcji="Koła ruletki"
    elif i[4]==wersja_selekcji:
        metoda_selekcji="Turniejowa"

    wersja_krzyz=int(dane[5])
    i = range(1,4)
    if i[0]==wersja_krzyz:
        metoda_krzyz="Liniowe"
    elif i[1]==wersja_krzyz:
        metoda_krzyz="Liniowe podwójne"
    elif i[2]==wersja_krzyz:
        metoda_krzyz="Jednorodne"

    wersja_mutacja=int(dane[6])
    i = range(1,5)
    if i[0]==wersja_mutacja:
        metoda_mutacja="Równomierna"
    elif i[1]==wersja_mutacja:
        metoda_mutacja="W oparciu o pozycje"
    elif i[2]==wersja_mutacja:
        metoda_mutacja="Pojedynczej zamiany"
    elif i[3]==wersja_mutacja:
        metoda_mutacja="Przyległa wymiana"

    sposob_zakonczenia=int(dane[7])
    if sposob_zakonczenia==1:
        zakonczenie="Ilość iteracji"
    elif sposob_zakonczenia==2:
        zakonczenie="Błąd względny"
    elif sposob_zakonczenia==3:
        zakonczenie="Stały wynik"

    blad_wzgledny=float(dane[8])
    ilosc_naj_do_zakonczenia=int(dane[9])
    granicznaIteracja=int(dane[10])
    dol_zakr=int(dane[11])
    gor_zakr=int(dane[12])
    liczbafirm=int(dane[13])
    mplywacy=int(dane[14])
    cenaRano=float(dane[15])
    cenaPopoludnie=float(dane[16])
    cenaWieczor=float(dane[17])
    poprawaHumoru=float(dane[18])
    pogorszenieHumoru=float(dane[19])
    maksGodzinyFirma=int(dane[20])
    maksToryFirma=int(dane[21])
    dolnaCenaFirma=int(dane[22])
    gornaCenaFirma=int(dane[23])
    Ilosc_torow_dla_zwyklych_klientow=int(dane[24])
    wspolczynnikKary=float(dane[25])

    mGui3=Tk()
    mGui3.geometry('550x565+'+str(szerokosc)+'+'+str(wysokosc))
    mGui3.title('Algorytm Ewolucyjny')
    Label(text=' Wczytano następującą konfigurację ',font=('Helvetica',16)).grid(row=0,column=0,columnspan=2)
    Label(text='Populacja początkowa:',bg='gray88').grid(row=1,column=0)
    Label(text=str(ilosc_rodzicow),fg='red',bg='gray88').grid(row=1, column=1)

    Label(text='Czas symulacji basenu:',bg='gray88').grid(row=2,column=0)
    Label(text=str(ilosc_tyg)+tyg,fg='red',bg='gray88').grid(row=2,column=1)

    Label(text='Prawdopodobieństwo mutacji:',bg='gray88').grid(row=3,column=0)
    Label(text=str(prawdopodobienstwo_mutacji),fg='red',bg='gray88').grid(row=3,column=1)

    Label(text='Selekcja:',bg='gray88').grid(row=4,column=0)
    Label(text=str(metoda_selekcji),fg='red',bg='gray88').grid(row=4,column=1)
    Label(text='Krzyżowanie:',bg='gray88').grid(row=5,column=0)
    Label(text=str(metoda_krzyz),fg='red',bg='gray88').grid(row=5,column=1)
    Label(text='Mutacja:',bg='gray88').grid(row=6,column=0)
    Label(text=str(metoda_mutacja),fg='red',bg='gray88').grid(row=6,column=1)
    Label(text='Zakończenie:',bg='gray88').grid(row=7,column=0)
    Label(text=str(zakonczenie),fg='red',bg='gray88').grid(row=7,column=1)
    if sposob_zakonczenia==1:
        Label(text='Liczba iteracji:',bg='gray88').grid(row=8,column=0)
        Label(text=str(iteracje),fg='red',bg='gray88').grid(row=8,column=1)
    elif sposob_zakonczenia==2:
        Label(text='Wartość błędu względnego:',bg='gray88').grid(row=8,column=0)
        Label(text=str(blad_wzgledny),fg='red',bg='gray88').grid(row=8,column=1)

    elif sposob_zakonczenia==3:
        Label(text='Liczba iteracji stałego wyniku:',bg='gray88').grid(row=8,column=0)
        Label(text=str(ilosc_naj_do_zakonczenia),fg='red',bg='gray88').grid(row=8,column=1)
    Label(text='Minimum osób na 1tor/1h w 1. tyg:',bg='gray88').grid(row=11,column=0)
    Label(text=str(dol_zakr),fg='red',bg='gray88').grid(row=11,column=1)
    Label(text='Maximum osób na 1tor/1h w 1. tyg:',bg='gray88').grid(row=12,column=0)
    Label(text=str(gor_zakr),fg='red',bg='gray88').grid(row=12,column=1)
    Label(text='Liczba firm w tygodniu:',bg='gray88').grid(row=13,column=0)
    Label(text=str(liczbafirm),fg='red',bg='gray88').grid(row=13,column=1)
    Label(text='Liczba sportowców w tygodniu:',bg='gray88').grid(row=14,column=0)
    Label(text=str(mplywacy),fg='red',bg='gray88').grid(row=14,column=1)
    Label(text='Cena wstępu w godzinach porannych:',bg='gray88').grid(row=15,column=0)
    Label(text=str(cenaRano),fg='red',bg='gray88').grid(row=15,column=1)
    Label(text='Cena wstępu w godzinach popołudniowych:',bg='gray88').grid(row=16,column=0)
    Label(text=str(cenaPopoludnie),fg='red',bg='gray88').grid(row=16,column=1)
    Label(text='Cena wstępu w godzinach wieczornych:',bg='gray88').grid(row=17,column=0)
    Label(text=str(cenaWieczor),fg='red',bg='gray88').grid(row=17,column=1)
    Label(text='Liczba osób/tor po której następuje poprawa nastawienia klienta:',bg='gray88').grid(row=18,column=0)
    Label(text=str(poprawaHumoru),fg='red',bg='gray88').grid(row=18,column=1)
    Label(text='Liczba osób/tor po której następuje pogorszenie nastawienia klienta:',bg='gray88').grid(row=19,column=0)
    Label(text=str(pogorszenieHumoru),fg='red',bg='gray88').grid(row=19,column=1)
    Label(text='Maksymalna ilość godzin w rezerwcji firmy:',bg='gray88').grid(row=20,column=0)
    Label(text=str(maksGodzinyFirma),fg='red',bg='gray88').grid(row=20,column=1)
    Label(text='Maksymalna ilość torów w rezerwcji firmy:',bg='gray88').grid(row=21,column=0)
    Label(text=str(maksToryFirma),fg='red',bg='gray88').grid(row=21,column=1)
    Label(text='Minimalna cena za 1tor/1h rezerwacji firm:',bg='gray88').grid(row=22,column=0)
    Label(text=str(dolnaCenaFirma),fg='red',bg='gray88').grid(row=22,column=1)
    Label(text='Maksymalna cena za 1tor/1h rezerwacji firm:',bg='gray88').grid(row=23,column=0)
    Label(text=str(gornaCenaFirma),fg='red',bg='gray88').grid(row=23,column=1)
    Label(text='Minimalna liczba torów/h na dzień wykorzystawana wyłącznie przez klientów(kara):',bg='gray88').grid(row=24,column=0)
    Label(text=str(Ilosc_torow_dla_zwyklych_klientow),fg='red',bg='gray88').grid(row=24,column=1)
    Label(text='Współczynnik kary:',bg='gray88').grid(row=25,column=0)
    Label(text=str(wspolczynnikKary),fg='red',bg='gray88').grid(row=25,column=1)

    if sposob_zakonczenia in (2,3):
        Label(text='Graniczna Iteracja:',bg='gray88').grid(row=26)
        Label(text=str(granicznaIteracja),fg='red',bg='gray88').grid(row=26,column=1)
    mGui3.bind("<Return>",kasuj3)
    Label(text='').grid(row=27,column=1)
    Button(text='OK',command = kasuj31,fg='red',bg='white').grid(row=28,column=0,columnspan=2)
    mGui3.mainloop()
    print maksGodzinyFirma,maksToryFirma,dolnaCenaFirma,gornaCenaFirma



if dolnaCenaFirma==gornaCenaFirma:
    STALA_WARTOSC_CENY = True
else:
    STALA_WARTOSC_CENY = False
ILOSC_DNI = 70
proces(ilosc_rodzicow,ilosc_tyg,prawdopodobienstwo_mutacji,iteracje,wersja_selekcji,wersja_krzyz,wersja_mutacja,sposob_zakonczenia,blad_wzgledny,ilosc_naj_do_zakonczenia,granicznaIteracja,dol_zakr,gor_zakr,liczbafirm,mplywacy,cenaRano,cenaPopoludnie,cenaWieczor,maksGodzinyFirma,maksToryFirma,dolnaCenaFirma,gornaCenaFirma,Ilosc_torow_dla_zwyklych_klientow,wspolczynnikKary,poprawaHumoru,pogorszenieHumoru,STALA_WARTOSC_CENY,ILOSC_DNI)
