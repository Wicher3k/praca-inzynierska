# -*- coding: utf-8 -*-
__author__ = 'Roland'
from Evo_fun import len_os,len_firm,analiza_zysku2
from dod_funkcje import licz_sportowcow,licz_firm,analiza_wynikow,dni_tygodnia
import codecs


def stworz_podsumowanie(L,gl_iter,max_zarobek,iteracja,ilosc_tyg,ilosc_rodzicow,prawdopodobienstwo_mutacji,wersja_krzyz,wersja_mutacja,wersja_selekcji,sposob_zakonczenia,wszystkie_wyniki,wszystkie_cele,ILOSC_DNI,zli_ost,neu_ost,dob_ost,l_osob,cenaRano,cenaPopoludnie,cenaWieczor,maksGodzinyFirma,maksToryFirma,dolnaCenaFirma,gornaCenaFirma,Ilosc_torow_dla_zwyklych_klientow,wspolczynnikKary,elapsed):
    f=codecs.open(u"Analiza wyników.csv",encoding='cp1250',mode="wb")
    f.write("Parametry algorytmu;;;;;Kodowanie windows-1250\n\n")
    if ilosc_tyg==1:
        tyg=u" tydzień"
    elif ilosc_tyg in (2,3,4):
        tyg=" tygodnie"
    else:
        tyg=" tygodni"
    f.write("Czas symulacji basenu:;"+str(ilosc_tyg)+tyg+"\n")
    f.write("Liczba populacji startowej:;"+str(ilosc_rodzicow)+"\n")
    f.write(u"Ilość iteracji:;"+str(iteracja)+"\n")
    f.write(u"Prawdopodobieństwo mutacji:;"+str(prawdopodobienstwo_mutacji).replace('.',',')+"\n")
    i=range(1,4)
    if i[0]==wersja_krzyz:
        metoda_krzyz=u"Krzyżowanie liniowe"
    elif i[1]==wersja_krzyz:
        metoda_krzyz=u"Krzyżowanie liniowe podwójne"
    elif i[2]==wersja_krzyz:
        metoda_krzyz=u"Krzyżowanie jednorodne"
    f.write(u"Metoda krzyżowania:;"+metoda_krzyz+"\n")
    i = range(1,5)
    if i[0]==wersja_mutacja:
        metoda_mutacja=u"Mutacja równomierna"
    elif i[1]==wersja_mutacja:
        metoda_mutacja="Mutacja w oparciu o pozycje"
    elif i[2]==wersja_mutacja:
        metoda_mutacja="Mutacja pojedynczej zamiany"
    elif i[3]==wersja_mutacja:
        metoda_mutacja=u"Przyległa wymiana"
    f.write("Metoda mutacji:;"+metoda_mutacja+"\n")
    i = range(1,6)
    if i[0]==wersja_selekcji:
        metoda_selekcji=u"Selekcja wyłączona"
    elif i[1]==wersja_selekcji:
        metoda_selekcji="Selekcja elitarna"
    elif i[2]==wersja_selekcji:
        metoda_selekcji="Selekcja rankingowa"
    elif i[3]==wersja_selekcji:
        metoda_selekcji=u"Selekcja koła ruletki"
    elif i[4]==wersja_selekcji:
        metoda_selekcji="Selekcja turniejowa"
    f.write("Metoda selekcji:;"+metoda_selekcji+"\n")
    if sposob_zakonczenia==1:
        zakonczenie=u"Ilość iteracji"
    elif sposob_zakonczenia==2:
        zakonczenie=u"Błąd względny"
    elif sposob_zakonczenia==3:
        zakonczenie=u"Stały wynik w zadanej ilości iteracji"

    f.write(u"Sposób zakończenia algorytmu:;"+zakonczenie+"\n")
    f.write(u"Cena wstępu w godzinach porannych:;"+str(cenaRano).replace('.',',')+"\n")
    f.write(u"Cena wstępu w godzinach popołudniowych:;"+str(cenaPopoludnie).replace('.',',')+"\n")
    f.write(u"Cena wstępu w godzinach wieczornych:;"+str(cenaWieczor).replace('.',',')+"\n")
    f.write(u"Maksymalna ilość godzin jednej rezerwcji firm:;"+str(maksGodzinyFirma).replace('.',',')+"\n")
    f.write(u"Maksymalna ilość torów jednej rezerwcji firm:;"+str(maksToryFirma).replace('.',',')+"\n")
    f.write(u"Minimalna cena za 1h/1tor rezerwacji firm:;"+str(dolnaCenaFirma).replace('.',',')+"\n")
    f.write(u"Maksymalna cena za 1h/1tor rezerwacji firm:;"+str(gornaCenaFirma).replace('.',',')+"\n")
    f.write(u"Minimalna liczba torów/h na dzień wykorzystawana wyłącznie przez klientów(kara):;"+str(Ilosc_torow_dla_zwyklych_klientow).replace('.',',')+"\n")
    f.write(u"Wspolczynnik Kary:;"+str(wspolczynnikKary).replace('.',',')+"\n")
    f.write(u"Czas wykonania skryptu:;"+(str(elapsed)+" [s]").replace('.',',')+"\n")
    f.write("\n")
    f.write("Analiza najlepszego wyniku;\n")
    f.write("Iteracja:;"+str(gl_iter)+"\n")
    f.write(u"Dochód:;"+str(max_zarobek).replace('.',',')+"\n")
    zli,neu,dob=0,0,0
    for i in range(ILOSC_DNI):
        for j in range(len(L[i])):
            if L[i][j]=="Z":
                zli+=1
            elif L[i][j]=="N":
                neu+=1
            elif L[i][j]=="P":
                dob+=1

    f.write(u"Liczba indywidualnych klientów w poszczególnych tygodniach;\n")
    for i in range(len(l_osob)):
        f.write(u"Liczba indywidualnych klientów w "+str(i+1)+". tygodniu:;"+str(l_osob[i])+"\n")
    f.write("Populacja w pierwszym tygodniu;\n")
    f.write(u"Liczba klientów o dobrym humorze:;"+str(dob)+"\n")
    f.write(u"Liczba klientów o neutralnym humorze:;"+str(neu)+"\n")
    f.write(u"Liczba klientów o złym humorze;"+str(zli)+"\n")
    f.write("Populacja w ostatnim tygodniu;\n")
    f.write(u"Liczba klientów o dobrym humorze:;"+str(zli_ost)+"\n")
    f.write(u"Liczba klientów o neutralnym humorze:;"+str(neu_ost)+"\n")
    f.write(u"Liczba klientów o złym humorze;"+str(dob_ost)+"\n")
    wszystkie_tory=6*ILOSC_DNI
    f.write(u"Liczba wszystkich torów;"+str(wszystkie_tory)+"\n")
    f.write(u"Ilość firm;"+str(licz_firm(L))+"\n")
    f.write(u"Ilość sportowców;"+str(licz_sportowcow(L))+"\n")
    liczba_torow_sport,liczba_torow_firm=0,0
    ilosc_os=len_os(L)
    for i in range(ilosc_os+1,len(L)):
        if L[i][3]==0:
            liczba_torow_sport+=L[i][1]*L[i][2]
        elif L[i][3]!=0:
            liczba_torow_firm+=L[i][1]*L[i][2]
    f.write(u"Liczba zajętych torów przez firmy;"+str(liczba_torow_firm)+"\n")
    f.write(u"Liczba zajętych torów przez sportowców;"+str(liczba_torow_sport)+"\n")
    zajete_tory=0
    for i in range(len_os(L)+1,len(L)):
        zajete_tory+=L[i][2]*L[i][1]
    wolne_tory=wszystkie_tory-zajete_tory
    f.write(u"Ilość wolnych torów;"+str(wolne_tory)+"\n")
    f.write("\n")

    f.write(u"Analiza poszczególnych godzin\n")
    f.write(u"Godzina;Ilość osób\n")
    for i in range(ilosc_os): #ILOSC_DNI ma byc
         f.write(dni_tygodnia(i,"pocz")+";"+str(len(L[i]))+"\n")
    f.write("\n")
    f.write(u"Analiza poszczególnych firm\n")
    f.write(u"ln.;Rozpoczęcie;Zakończenie;ilość zajętych torów;zapłata za 1tor/1h\n")
    j=0
    for i in range(len_firm(L)):
        if L[ilosc_os+1+i][3]!=0:
            f.write(str(j+1)+";"+dni_tygodnia(L[ilosc_os+1+i][0],"pocz")+";"+dni_tygodnia(L[ilosc_os+1+i][1]+L[ilosc_os+1+i][0],"zako")+";"+str(L[ilosc_os+1+i][2])+";"+str(L[ilosc_os+1+i][3])+"\n") # nalezy pozmieniac na pon/ itd
            j+=1
    f.write("\n")
    f.write(u"Analiza poszczególnych sportowców\n")
    f.write(u"ln.;Rozpoczęcie;Zakończenie;Ilość zajętych torów\n")
    j=0
    for i in range(len_firm(L)):
        if L[ilosc_os+1+i][3]==0:
            f.write(str(j+1)+";"+dni_tygodnia(L[ilosc_os+1+i][0],"pocz")+";"+dni_tygodnia(L[ilosc_os+1+i][1]+L[ilosc_os+1+i][0],"zako")+";"+str(L[ilosc_os+1+i][2])+";\n")
            j+=1
    f.write("\n \n")

    f.write(u"Analiza wszystkich wyników;\n")
    gorszy,sredni,lepszy=analiza_zysku2(wszystkie_cele)
    f.write(u"Największy dochód:;"+str(lepszy).replace('.',',')+"\n")
    f.write(u"Średnia dochód:;"+str(sredni).replace('.',',')+"\n")
    f.write(u"Najgorszy dochód:;"+str(gorszy).replace('.',',')+"\n")
    wie_f,wie_s,sre_f,sre_s,mni_f,mni_s,wie_zad,sre_zad,mni_zad,wie_neu,sre_neu,mni_neu,wie_zly,sre_zly,mni_zly=analiza_wynikow(wszystkie_wyniki)
    f.write(u"Największa liczba zajętych torów przez firmy:;"+str(wie_f).replace('.',',')+"\n")
    f.write(u"Średnia liczba zajętych tórow przez firmy:;"+str(sre_f).replace('.',',')+"\n")
    f.write(u"Najmniejsza liczba zajętych torów przez firmy:;"+str(mni_f).replace('.',',')+"\n")
    f.write(u"Największa liczba zajętych torów przez sportowców:;"+str(wie_s).replace('.',',')+"\n")
    f.write(u"Średnia liczba zajętych torów przez sportowców:;"+str(sre_s).replace('.',',')+"\n")
    f.write(u"Najmniejsza liczba zajętych torów przez sportowców:;"+str(mni_s).replace('.',',')+"\n")
    f.write(u"Największa liczba zadowolonych klientow w 1-szym tyg;"+str(wie_zad).replace('.',',')+"\n")
    f.write(u"Średnia liczba zadowolonych klientow w 1-szym tyg :;"+str(sre_zad).replace('.',',')+"\n")
    f.write(u"Najmniejsza liczba zadowolonych klientow w 1-szym tyg:;"+str(mni_zad).replace('.',',')+"\n")
    f.write(u"Największa liczba klientow o neutralnym humorze 1-szym tyg:;"+str(wie_neu).replace('.',',')+"\n")
    f.write(u"Średnia liczba klientow o neutralnym humorze 1-szym tyg:;"+str(sre_neu).replace('.',',')+"\n")
    f.write(u"Najmniejsza liczba klientow o neutralnym humorze 1-szym tyg:;"+str(mni_neu).replace('.',',')+"\n")
    f.write(u"Największa liczba złych klientow w 1-szym tyg:;"+str(wie_zly).replace('.',',')+"\n")
    f.write(u"Średnia liczba złych klientow w 1-szym tyg:;"+str(sre_zly).replace('.',',')+"\n")
    f.write(u"Najmniejsza liczba złych klientow w 1-szym tyg:;"+str(mni_zly).replace('.',',')+"\n")
if  __name__ == '__main__':
    L=[[['Z','Z'],['N','P','P',"P"],['N'],'przerwa',[2,5,6,5],[2,1,2,5],[2,2,2,0],[2,1,1,0]],[['P','P'],['N'],['Z'],'przerwa',[2,3,1,0],[2,3,1,0],[2,2,1,2],[2,2,2,2]]]
    gl_iter=1
    max_zarobek=200
    iteracja=2
    ilosc_tyg=2
    ilosc_rodzicow=2
    prawdopodobienstwo_mutacji=3
    wersja_krzyz=2
    wersja_mutacja=2
    wersja_selekcji=3
    sposob_zakonczenia=1
    wszystkie_wyniki=L
    wszystkie_cele=[(200,[820, 793, 110], [1400, 1677, 1723, 1723]),(100,[199],[2131])]
    ILOSC_DNI=5
    zli_ost=1
    neu_ost=2
    dob_ost=3
    cenaRano=8.1
    cenaPopoludnie=9.2
    cenaWieczor=11.1

    stworz_podsumowanie(L[0],gl_iter,max_zarobek,iteracja,ilosc_tyg,ilosc_rodzicow,prawdopodobienstwo_mutacji,wersja_krzyz,wersja_mutacja,wersja_selekcji,sposob_zakonczenia,wszystkie_wyniki,wszystkie_cele,ILOSC_DNI,zli_ost,neu_ost,dob_ost,(2,3,4,5,6),cenaRano,cenaPopoludnie,cenaWieczor)