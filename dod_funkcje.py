__author__ = 'Roland'
# -*- coding: utf-8 -*-
from Evo_fun import len_os
import copy


def licz_sportowcow(L):
    l_sport=0
    liczba_os=len_os(L)
    for i in range(liczba_os+1,len(L)):
        if L[i][3]==0 :l_sport+=1
    return l_sport

def licz_firm(L):
    l_firm=0
    liczba_os=len_os(L)
    for i in range(liczba_os+1,len(L)):
        if L[i][3]!=0 :l_firm+=1
    return l_firm

"""

def licz_osoby(Lpocz,ilosctygodni): # liczy osoby przez dana ilosc tygodni
    wektor_l_os=[]
    L=[]
    L=copy.deepcopy(Lpocz)

    for tydzien in range(ilosctygodni):
        for i in range(len_os(L)):
            suma=0      #suma zajetych torow
            for j in range(len_firm(L)):
                for m in range(L[len_os(L)+j+1][1]):
                    if (L[len_os(L)+j+1][0]+m) == i: suma+=L[len_os(L)+1+j][2] #jak znaleziono to licz ile zajetych torow
            if suma==6:
                L[i]=None # wsyzscy niezaleznie od humoru zostaja wyrzuceni i nie przyjda
            else:
                for j in range(len(L[i])):
                    if (len(L[i])/(6-suma))>=8:# !!!!!!
                        if L[i][j] == 'N':
                            L[i][j] = 'Z'
                        elif L[i][j] == 'P':
                            L[i][j] = 'N'
                        elif L[i][j] == 'Z':
                            L[i][j] = 'dowyrzutu'
                        else: print('BUG')
                    if (len(L[i])/(6-suma))<=5: #!!!!!!
                        if L[i][j] == 'N':
                            L[i][j] = 'P'
                        elif L[i][j] == 'P':
                            L[i][j] = 'AAA'
                        elif L[i][j] == 'Z':
                            L[i][j] = 'N'
                        else: print('BUG')


        wektor_l_os.append(licz_os(L))
        migracja_ludzi(L)

    return wektor_l_os

def analiza_zysku(wszystkie_cele):
    mini,srednia,maxi=1000000,0,0
    for cel in wszystkie_cele:
        if cel>maxi:
            maxi=cel
        elif cel<mini:
            mini=cel
        srednia+=cel
    sred=float(srednia)/len(wszystkie_cele)
    return maxi,sred,mini"""


def analiza_wynikow(wszystkie_wyniki):
    wie_f,wie_s,sre_f,sre_s,mni_f,mni_s,wie_zad,sre_zad,mni_zad,wie_neu,sre_neu,mni_neu,wie_zly,sre_zly,mni_zly=0,0,0,0,1000000,1000000,0,0,1000000,0,0,1000000,0,0,1000000
    srednia_zad,srednia_neu,srednia_zly=0,0,0
    srednia_sport,srednia_firm=0,0
    for L in wszystkie_wyniki:
        licz_zly,licz_neu,licz_zad=0,0,0
        for i in range(len_os(L)):
            for gen in L[i]:
                if gen=="Z":
                    licz_zly+=1
                elif gen=="N":
                    licz_neu+=1
                elif gen=="P":
                    licz_zad+=1
        if licz_zad>wie_zad:
            wie_zad=licz_zad
        if licz_zad<mni_zad:
            mni_zad=licz_zad
        srednia_zad+=licz_zad

        if licz_neu>wie_neu:
            wie_neu=licz_neu
        if licz_neu<mni_neu:
            mni_neu=licz_neu
        srednia_neu+=licz_neu

        if licz_zly>wie_zly:
            wie_zly=licz_zly
        if licz_zly<mni_zly:
            mni_zly=licz_zly
        srednia_zly+=licz_zly

        liczba_torow_sport,liczba_torow_firm=0,0
        for i in range(len_os(L)+1,len(L)):
            if L[i][3]==0:
                liczba_torow_sport+=L[i][1]*L[i][2]
            elif L[i][3]!=0:
                liczba_torow_firm+=L[i][1]*L[i][2]

        if liczba_torow_sport>wie_s:
            wie_s=liczba_torow_sport
        if liczba_torow_sport<mni_s:
            mni_s=liczba_torow_sport
        srednia_sport+=liczba_torow_sport

        if liczba_torow_firm>wie_f:
            wie_f=liczba_torow_firm
        if liczba_torow_firm<mni_f:
            mni_f=liczba_torow_firm
        srednia_firm+=liczba_torow_firm


    sre_zad=float(srednia_zad)/len(wszystkie_wyniki)
    sre_neu=float(srednia_neu)/len(wszystkie_wyniki)
    sre_zly=float(srednia_zly)/len(wszystkie_wyniki)
    sre_s=float(srednia_sport)/len(wszystkie_wyniki)
    sre_f=float(srednia_firm)/len(wszystkie_wyniki)
    return wie_f,wie_s,sre_f,sre_s,mni_f,mni_s,wie_zad,sre_zad,mni_zad,wie_neu,sre_neu,mni_neu,wie_zly,sre_zly,mni_zly

def dni_tygodnia(nr,kiedy):
    i=14
    for k in range(7):
        if nr==(0+k*i):
            if kiedy=="pocz":
                tydzien=" 8:00"
            elif kiedy=="zako":
                tydzien=" 22:00"
                k-=1
            break
        elif nr==(1+k*i):
            tydzien=" 9:00"
            break
        elif nr==(2+k*i):
            tydzien=" 10:00"
            break
        elif nr==(3+k*i):
            tydzien=" 11:00"
            break
        elif nr==(4+k*i):
            tydzien=" 12:00"
            break
        elif nr==(5+k*i):
            tydzien=" 13:00"
            break
        elif nr==(6+k*i):
            tydzien=" 14:00"
            break
        elif nr==(7+k*i):
            tydzien=" 15:00"
            break
        elif nr==(8+k*i):
            tydzien=" 16:00"
            break
        elif nr==(9+k*i):
            tydzien=" 17:00"
            break
        elif nr==(10+k*i):
            tydzien=" 18:00"
            break
        elif nr==(11+k*i):
            tydzien=" 19:00"
            break
        elif nr==(12+k*i):
            tydzien=" 20:00"
            break
        elif nr==(13+k*i):
            tydzien=" 21:00"
            break
    if k==0:
        tydzien="Pon"+tydzien
    elif k==1:
        tydzien="Wt"+tydzien
    elif k==2:
        tydzien="Sr"+tydzien
    elif k==3:
        tydzien="Czw"+tydzien
    elif k==4:
        tydzien="Pt"+tydzien
    elif k==5:
        tydzien="Sb"+tydzien
    elif k==6:
        tydzien="Nd"+tydzien
    return tydzien



if  __name__ == '__main__':
    L=[[['Z','Z'],['N','P','P',"P"],['N'],'przerwa',[2,1,6,5],[2,1,2,5],[1,1,1,1],[2,2,2,0],[2,1,1,0]],[['P','P'],['N'],['Z'],'przerwa',[2,3,1,0],[2,3,1,0],[2,2,1,2],[2,2,2,2]]]
    zarobek=1000
    liczbatyg=3
    #sportowcy=licz_sportowcow(L[0])
    #firmy=licz_firm(L[0])
    #osoby,Z,N,D=licz_osoby(L,liczbatyg)
    #print('Liczba sportowcow to:',sportowcy)
    #print('liczba firm to:',firmy)
    print dni_tygodnia(14,"zako")
    """
    for i in range(liczbatyg):
        print('osoby w tyg nr:'+str(i+1),osoby[i])

    cele=[100,10,200,100]
    a,b,c=analiza_zysku(cele)

    wie_f,wie_s,sre_f,sre_s,mni_f,mni_s,wie_zad,sre_zad,mni_zad,wie_neu,sre_neu,mni_neu,wie_zly,sre_zly,mni_zly=analiza_wynikow(L)
    print wie_zad,sre_zad,mni_zad
    print wie_neu,sre_neu,mni_neu
    print wie_zly,sre_zly,mni_zly"""
