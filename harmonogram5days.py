__author__ = 'Roland'
__author__ = 'Roland'
# -*- coding: utf-8 -*-
from Tkinter import *
from Evo_fun import len_firm, len_os
from dod_funkcje import licz_firm,licz_sportowcow

def kasuj1(event):
    mGui.destroy()

def stworz_portal():
        global mGui, ment
        mGui=Tk()
        mGui.geometry('600x580+400+200')

def harmonogram(L,max_zarobek,liczbatygodni,gl_iter,l_osob):
    dlugoscOs=len_os(L)
    max_zarobek_str=str(max_zarobek)
    try:
        a=max_zarobek_str.index(".")
        try:
            x=max_zarobek_str[a+2]
        except:
            max_zarobek_str+='0'
    except:
        pass

    stworz_portal()
    mGui.title('Harmonogram basenu')
    x=0
    M=[]
    for i in range(16):
        M.append(list(range(45)))
    p_w_d=2
    mlabel=Label(text=' ').grid(row=0,column=0,sticky=S)
    for i in range(45):
        if x==0 or x==7 or x==8:
            mlabel=Label(text='').grid(row=1,column=i+5,sticky=W)
        else:
            mlabel=Label(text=str(x)+" ",fg='red',bg='gray88').grid(row=0+p_w_d,column=i+5,sticky=S)
        if x==8: x=-1
        x+=1
    for i in range(14):
        mlabel=Label(text=str(i+8)+" ",fg='red',bg='gray88').grid(row=i+1+p_w_d,column=5,sticky=E)

    for blankpoz in range(44):
        if blankpoz==6 or blankpoz==7 or blankpoz==8 or blankpoz==15 or blankpoz==16 or blankpoz==17or blankpoz==24 or blankpoz==25 or blankpoz==26 or blankpoz==33 or blankpoz==34 or blankpoz==35 or blankpoz==42 or blankpoz==43 or blankpoz==44 or blankpoz==51 or blankpoz==52 or blankpoz==53: continue
        for blankpion in range(14):
            mlabel=Label(text='  ',bg='gray75').grid(row=3+blankpion,column=blankpoz+1+5,sticky=E)

    for firma in range(len_firm(L)):
        dzien=(L[firma+dlugoscOs+1][0])//14
        x=0
        for godziny in range(L[firma+dlugoscOs+1][1]):
            tory=0
            while tory < L[firma+dlugoscOs+1][2]:
                kolumna=tory+dzien*9+x
                y=L[firma+dlugoscOs+1][0]%14+1+godziny
                if  M[y][kolumna+1]=='zajete':
                    x+=1
                    continue
                if L[firma+dlugoscOs+1][3]==0:
                    mlabel=Label(text='  ',bg='green').grid(row=L[firma+dlugoscOs+1][0]%14+1+p_w_d+godziny,column=kolumna+1+5,sticky=E)
                else:
                    mlabe2=Label(text='  ',bg='yellow').grid(row=L[firma+dlugoscOs+1][0]%14+1+p_w_d+godziny,column=kolumna+1+5,sticky=E)
                M[y][kolumna+1]='zajete'
                tory+=1

    mlabel1=Label(text=u'Doch\xf3d : ' +max_zarobek_str +u' z\u0142',font=('Helvetica',20), fg='red',bg='gray88').place(x=200,y=0)

    mlabe0=Label(text='Legenda : ',font=('Helvetica',12), fg='red',bg='gray88').place(x=20,y=370)
    mlabel=Label(text=' ',bg='gray75').place(x=20,y=400)
    mlabel=Label(text=' ',bg='yellow').place(x=20,y=420)
    mlabe2=Label(text=' ',bg='green').place(x=20,y=440)
    mlabel0=Label(text='- wolne tory',bg='gray88').place(x=29,y=400)
    mlabel1=Label(text=u'- zaj\u0119te tory przez firmy ',bg='gray88').place(x=29,y=420)
    mlabe22=Label(text=u'- zaj\u0119te tory przez sportowc\xf3w ',bg='gray88').place(x=29,y=440)

    l_sportowcy=licz_sportowcow(L)
    l_firmy=licz_firm(L)
    mlabe0=Label(text='Liczba sportowc\xf3w : '+str(l_sportowcy), fg='red',bg='gray88').place(x=400,y=370)
    mlabe0=Label(text='Liczba firm : '+str(l_firmy), fg='red',bg='gray88').place(x=400,y=390)
    for i in range(liczbatygodni):
        mlabell=Label(text='Liczba os\xf3b w tygodniu nr: '+str(i+1)+' to: '+str(l_osob[i]), fg='red',bg='gray88').place(x=400,y=410+i*20)

    label=Label(text=u'Najwi\u0119ksza warto\u015b\u0107 to '+max_zarobek_str+u' z\u0142 w g\u0142\xf3wnej iteracji : '+str(gl_iter),fg='red',bg='gray88').place(x=20,y=480)
    mGui.bind("<Return>",kasuj1)
    mainloop()
if  __name__ == '__main__':
    L=[['P','N'],['N'],'przerwa',[13,1,1,1]]
    zarobek=1000
    liczbatygodni=4
    gl_iter=2
    harmonogram(L,zarobek,liczbatygodni,gl_iter,(2,3,4,5,5,5,5,5,5,5,5,3))
